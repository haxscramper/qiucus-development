#!/usr/bin/env perl
use lib '..';

use warnings;
use strict;
use v5.20;
use File::Copy;
use Cwd;
use File::Path qw(make_path remove_tree);
use Switch;
my $script_dir = getcwd;
my $project_dir = "$script_dir/..";

use Pod::Usage;
use Getopt::Long;

use constant false => 0;
use constant true  => 1;

use feature qw(signatures);
no warnings qw(experimental::signatures);

use scripts::common qw(print_error print_status_small print_status_big print_warning print_status);

my $help;
my $build_all;
my $build_docs;
my $build_qscintilla;
my $build_qdelib;
my $build_qwgtlib;


GetOptions(
    "help" => \$help,
    "all"  => \$build_all,
    "docs" => \$build_docs,
    "qscintilla" => \$build_qscintilla,
    "qdelib" => \$build_qdelib,
    "qwgtlib" => \$build_qwgtlib,
) or pod2usage( -verbose => 1 ) && exit 0;

if ( defined $help ) {
    pod2usage( -verbose => 1 ) && exit 0;
}

if (defined $build_docs or defined $build_all) {
    chdir $project_dir;
    system ("./external/m.css/doxygen/dox2html5.py scripts/Doxyfile-mcss");
}

if (defined $build_qscintilla or defined $build_all ) {
    print_status_big("Building qscintilla");

    chdir $project_dir;
    chdir "external/qscintilla-backup/Qt4Qt5";
    system ("qmake");
    system ("make --quiet -j10");
    copy("libqscintilla2_qt5.so", "../../../source");

    print_status_big("Done building qscintilla");
}

if (defined $build_qdelib or defined $build_all) {
    chdir $project_dir;
    chdir "source/qdelib";
    system ("./build.pl --global-test");
}

if (defined $build_qwgtlib or defined $build_all) {
    chdir $project_dir;
    chdir "source/qwgtlib";
    system ("./build.sh");
}


=head1 NAME

build.pl

=head1 SYNOPSIS

build.pl [OPTIONS]

=head1 OPTIONS

--help -h      Print help message
--all -a       Build absolutely everythin

=cut
