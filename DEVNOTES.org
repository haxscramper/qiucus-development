** Makefiles

Makefiles in top-level directories are not used to actually _build_
anything, I use them instead of 10s of scripts for things like
checking whether or not software is availiable, configuring project as
a whole etc.

All makefiles have (or at least should have) target "help": it lists
all documentation comments for all targets in the makefile.

There is a "init" target in the master project: it checks if all the
required software is availiable (software, libraries, utilities etc.)

*** git-init

"git-init" target in the master file is made for configuring
development: if you run ~make git-init type=dev proj=qdelib~ (for
example) after cloning it will enable git flow in all submodules and
start ~feature/dev-misc~ in all of them except qdelib. This is made
for simplifying development process: if you want to work on particular
submodule but might want to make edits in the others but you'd better
just pack all of the things you changed there into patches.

*** git-status

Print status of all submodules + toplevel project.

** run-dev.sh & test.sh

In a lot of directories with scripts you might find ~run-dev.sh~,
~test.sh~ and sometimes ~build.sh~ scripts: they are useful only for
development. The concept is pretty simple: each time file with script
changes, run some simple test on it. In most cases it comes down to
running the script as if it was actually used: there is no complicated
unit-tests, edge case checkign and so on. The only purpose is to
_quickly_ get feedback on changes I made without making any additional
changes.
